var app=angular.module('Tobi',[]);
// this is where i defined my application module and gave it a name called tobi 
// then saved it in a variable called app 



// created a controller and connected it to the app
app.controller('SamCtrl',function($scope){
	// created a controller called SamCtrl
	// defined a parameter called $scope which is used to bind the view to the controller
	$scope.doSubmit= function(){
		// doSubmit os a self created function
		// used console.log() serviceto output or print out the user input to the browser console
     	console.log($scope.Surname);
		console.log($scope.Firstname);
		console.log($scope.OtherNames);
		console.log($scope.Username);
		console.log($scope.Hobby);
	}
})

